from flask import render_template, request
from jwt import JWT
from datetime import datetime as DT
import datetime

from werkzeug.utils import redirect

from app import app
from app.model import ToDo, db


# # todo replace with db later - Done. Leaving here for testing
# my_data = [
#     {
#         "title": "Todo 1",
#         "desc": "This is a description",
#         "status": "Open",
#         "created_date": "2020/05/01",
#         "due_date": "2020/05/29"
#     },
#     {
#         "title": "Todo 2",
#         "desc": "This is a description",
#         "status": "WIP",
#         "created_date": "2020/04/01",
#         "due_date": "2020/04/29"
#     },
#     {
#         "title": "Todo 3",
#         "desc": "This is a description",
#         "status": "Closed",
#         "created_date": "2020/01/01",
#         "due_date": '2020/02/29'
#     }
#
# ]
#

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


# todo fix login later
@app.route('/login')
def get_token():
    jwt = JWT()
    expiration_date = DT.utcnow() + datetime.timedelta(seconds=400)
    token = jwt.encode(payload={'exp': expiration_date}, alg='HS256')

    return token


@app.route('/all', methods=['GET'])
def get_all_todos():
    all_todos = ToDo.query.order_by(ToDo.due_date).all()
    return render_template('all_todos.html', todos=all_todos)
    # return render_template('all_todos.html')


def validate_title(form):
    if not form['title']:
        return 'title'


def validate_due_date(form):
    if not form['due_date']:
        return 'due_date'


def validate_input(form):
    error_stack = []
    if validate_title(form):
        error_stack.append('Title')
    if validate_due_date(form):
        error_stack.append('Due Date')
    return error_stack


@app.route('/todo', methods=['POST', 'GET'])
def create_todo():
    new_todo = ToDo()
    if request.method == 'POST':
        error_stack = validate_input(request.form)
        if error_stack:
            return render_template('create.html', error=error_stack)

        new_todo.title = request.form['title']
        new_todo.description = request.form['desc']
        new_todo.due_date = DT.strptime(request.form['due_date'], '%Y-%m-%d')
        db.session.add(new_todo)
        db.session.commit()
        return redirect('/all')

    else:
        return render_template('create.html')


@app.route('/todo/<int:id>/edit', methods=['POST', 'GET'])
def update_todo(id):
    todo = ToDo.query.get_or_404(id)

    if request.method == 'POST':
        error_stack = validate_input(request.form)
        if error_stack:
            return render_template('edit.html', error=error_stack, todo=todo)

        todo.title = request.form['title']
        todo.description = request.form['desc']
        todo.status = request.form['status']
        todo.due_date = DT.strptime(request.form['due_date'], '%Y-%m-%d')
        db.session.commit()
        return redirect('/all')

    elif request.method == 'GET':
        return render_template('edit.html', todo=todo)


@app.route('/todo/<int:id>/delete')
def delete_todo(id):
    todo = ToDo.query.get_or_404(id)
    db.session.delete(todo)
    db.session.commit()
    return redirect('/all')
