from flask_sqlalchemy import SQLAlchemy
from datetime import datetime as DT
from . import app

db = SQLAlchemy(app)


class ToDo(db.Model):
    id = db.Column("id", db.Integer, db.Sequence('id'), primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(200), nullable=True)
    due_date = db.Column(db.DateTime, nullable=True)
    status = db.Column(db.String(10), default="New")
    created_date = db.Column(db.DateTime, default=DT.utcnow)

    def __repr__(self):
        return 'To do ' + str(self.id) + str(self.title)
