from app import app
from app.model import db

app.run(port=5000, debug=True)

with app.app_context():
    db.create_all()
